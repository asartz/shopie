<?php

// this file should NEVER be placed into source control.
// i intentionally left the file unders source control so that the downloader can easily setup the app on a vagrant box.

return array(

	'CLIENT_FIRM'                 => 'Shopie',
	'PGSQL_HOST'                  => 'localhost',
	'PGSQL_DATABASE'              => 'shopie',
	'PGSQL_USERNAME'              => 'homestead',
	'PGSQL_PASSWORD'              => 'secret',
	'AWS_KEY'                     => '',
	'AWS_SECRET'                  => '',
	'AWS_REGION'                  => 'eu-west-1',
	'AWS_BUCKET'                  => '',
	'AWS_FOLDER_IN_BUCKET'        => '',
	'AWS_IMAGES_FOLDER_IN_BUCKET' => '',

);