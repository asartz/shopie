## Shopie - a little shop

Shopie is an eshop application built on Laravel framework for demonstration purposes.


## Requirements

After cloning the project, create a postgresql database named shopie on your dev box. 
The user / password for the database should be homestead / secret or whatever you wish, as long as you correctly setup the environment of your app (look at .env.local.php file at the project root. You may need to copy it to .env.php and fill in your credentials)

It should be fairly easy especially if you are running a homestead vagrant box.

Don't forget to do a composer update to fetch the various packages to your vendor dir.

And finally migrate & seed the database by doing a php artisan migrate --seed
  
### Keypoints ###

* mvc approach
* bower package management
* responsive layout using twitter bootstrap
* seo friendly urls

### Features ###

* Backend administration (products/ categories/ brands / coupons)
* Shopping cart with coupons
* New customer registration with shipping and billing address
* Customers orders history
* Profile management

### Todo ###

* add file management for product images (aws s3) currently using placehold.it for demonstration
* multiple shipping / billing addresses
* attach a prettier theme
* implement payment methods
* multiple languages / currencies
* implement tests