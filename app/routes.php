<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
 */

Route::get('/', ['uses' => 'HomeController@index', 'as' => 'index']);

Route::get('contact', ['uses' => 'HomeController@contact', 'as' => 'contact']);

Route::post('contact', ['uses' => 'HomeController@contactrequest', 'as' => 'contactrequest']);

Route::get('preview/{id}', ['uses' => 'HomeController@preview', 'as' => 'preview']);

Route::get('cart/add/{id}', ['uses' => 'CartController@cartadd', 'as' => 'cartadd']);

Route::get('cart/remove/{id}', ['uses' => 'CartController@cartremove', 'as' => 'cartremove']);

Route::get('cart/coupon/add/{coupon?}', ['uses' => 'CartController@cartcouponadd', 'as' => 'cartcouponadd']);

Route::get('cart/coupon/remove/{coupon?}', ['uses' => 'CartController@cartcouponremove', 'as' => 'cartcouponremove']);

Route::get('cart/update/{id}/{qty}', ['uses' => 'CartController@cartupdate', 'as' => 'cartupdate']);

Route::get('cart/checkout', ['uses' => 'CartController@cartcheckout', 'as' => 'cartcheckout']);

Route::get('cart/register', ['uses' => 'CartController@cartregister', 'as' => 'cartregister']);

Route::post('cart/signup', ['uses' => 'UserController@signup', 'as' => 'signup']);

Route::group(['before' => 'auth'], function () {
    Route::get('cart/shippingaddress', ['uses' => 'UserController@shippingaddress', 'as' => 'shippingaddress']);

    Route::post('cart/shippingaddress', ['uses' => 'UserController@postshippingaddress', 'as' => 'postshippingaddress']);

    Route::post('cart/billingaddress', ['uses' => 'UserController@postbillingaddress', 'as' => 'postbillingaddress']);

    Route::get('orders', ['uses' => 'UserController@orders', 'as' => 'myorders']);

    Route::get('profile', ['uses' => 'UserController@profile', 'as' => 'profile']);

    Route::post('profile', ['uses' => 'UserController@postprofile', 'as' => 'postprofile']);

    Route::get('placeorder', ['uses' => 'UserController@placeorder', 'as' => 'placeorder']);

    Route::post('placeorder', ['uses' => 'UserController@postplaceorder', 'as' => 'postplaceorder']);
});

Route::get('forbidden', array('uses' => 'UserController@forbidden', 'as' => 'forbidden'));

Route::get('login', ['uses' => 'AuthController@getLogin', 'as' => 'login']);

Route::post('login', ['uses' => 'AuthController@postLogin', 'as' => 'authenticate']);

Route::get('logout', ['uses' => 'AuthController@getLogout', 'as' => 'logout']);

Route::get('forbidden', ['uses' => 'AuthController@forbidden', 'as' => 'forbidden']);

Route::group(['before' => 'auth|manageparameters'], function () {
    Route::resource('user', 'UserController');

    Route::resource('category', 'CategoryController');

    Route::resource('brand', 'BrandController');

    Route::resource('coupon', 'CouponController');

    Route::resource('loginparam', 'LoginController');
});

Route::group(['before' => 'auth|managecontent'], function () {
    Route::resource('product', 'ProductController');
});

Route::group(array('before' => 'auth|usertypeadminstrict'), function () {
    Route::get('adminuserlist', ['uses' => 'UserController@adminuserlist', 'as' => 'adminuserlist']);

    // caution! this is a very dangerous route. it is used to enable admin user to login as a different user.
    Route::get('loginusingid/{id}', array('as' => 'loginusingid', 'uses' => 'UserController@loginusingid'))->where('id', '[0-9]+');
});