<?php

class Custom {
	/**
	 * Returns current date time formatted for database with miliseconds
	 *
	 * @return string
	 */
	public static function dbTimeStamp()
	{
		$time = microtime(true);
		$micro_time = sprintf("%06d", ($time - floor($time)) * 1000000);
		$dt   = new DateTime(date('Y-m-d H:i:s.'.$micro_time, $time));
		return $dt->format('Y-m-d H:i:s.u');
	}

	/**
	 * Returns current date time formatted for database without miliseconds
	 *
	 * @return string
	 */
	public static function dbTimeStampShort()
	{
		$dt = new DateTime();
		return $dt->format('Y-m-d H:i:s');
	}

	/**
	 * Returns input date formatted 'Y-m-d H:i:s' for display without miliseconds
	 *
	 * @param date $datetime
	 * @return string
	 */
	public static function dateFormat($datetime)
	{
		if ( ! empty($datetime))
		{
			$dt = new DateTime($datetime);
			return $dt->format('d/m/Y H:i:s');
		}
		return $datetime;
	}

	/**
	 * Returns input date formatted 'Y-m-d' for display
	 *
	 * @param date $datetime
	 * @return string
	 */
	public static function dateFormatShort($datetime)
	{
		if ( ! empty($datetime))
		{
			$dt = new DateTime($datetime);
			return $dt->format('d/m/Y');
		}
		return $datetime;
	}
}
