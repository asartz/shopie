<?php

use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;

class Product extends Eloquent implements SluggableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'products';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * Sluggable package functionality for this model.
	 *
	 */
	use SluggableTrait;
/*
protected $sluggable = array(
'build_from' => 'modeltitle',
'save_to'    => 'slug',
);
*/

	protected $sluggable = array(
		'build_from' => 'modelfulltitle',
		'save_to'    => 'slug',
	);

	public function getModelfulltitleAttribute()
	{
		return $this->category->descr.' '.$this->brand->descr.' '.$this->modeltitle.' '.$this->id;
	}

	public function category()
	{
		return $this->belongsTo('Category');
	}

	public function brand()
	{
		return $this->belongsTo('Brand');
	}

}
