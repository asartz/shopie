<?php

class Orderdetail extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'orderdetails';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	public function order()
	{
		return $this->belongsTo('Order');
	}

	public function product()
	{
		return $this->belongsTo('Product');
	}

}
