<?php

class Productlist extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'videos';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	public function category()
	{
		return $this->belongsTo('Category');
	}

	public function subcategory()
	{
		return $this->belongsTo('Subcategory');
	}

	public function cameraman()
	{
		return $this->belongsTo('Cameraman');
	}

	public function classification()
	{
		return $this->belongsTo('Classification');
	}

	public function level()
	{
		return $this->belongsTo('Level');
	}

	public function owner()
	{
		return $this->belongsTo('User', 'owner_id');
	}

	public function getDtinAttribute($value)
	{
		return date('d/m/Y', strtotime($value));
	}

	public function scopeOwned($query)
	{
		return $query->where('owner_id', Custom::owner());
	}

}
