<?php

class Coupon extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'coupons';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/*
	 * Accessors & Mutators
	*/

	public function getValiddatefromAttribute($value)
	{
		return Custom::dateFormatShort($value);
	}

	public function getValiddatetoAttribute($value)
	{
		return Custom::dateFormatShort($value);
	}

}
