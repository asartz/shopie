<?php

use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;

class Brand extends Eloquent implements SluggableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'brands';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * Sluggable package functionality for this model.
	 *
	 */
	use SluggableTrait;

	protected $sluggable = array(
		'build_from' => 'descr',
		'save_to'    => 'slug',
	);
}
