<?php

class Order extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'orders';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	public function orderdetails()
	{
		return $this->hasMany('Orderdetail');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function coupon()
	{
		return $this->belongsTo('Coupon');
	}

}
