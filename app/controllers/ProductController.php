<?php

class ProductController extends BaseController {
	/**
	 * Make sure we are secure at row level
	 */
	public function __construct()
	{
		$this->beforeFilter(function($route, $request)
		{
			if (count(Product::find($route->getParameter('product', 0))) === 0)
			{
				return Redirect::route('forbidden');
			}
		}, ['only' => ['show', 'edit', 'update', 'destroy']]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$records = Product::orderBy('id', 'desc')->with('category')->with('brand')->get();

		return View::make('product.index')->with(compact('records'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$categories = ['' => 'Select Category'] + Category::orderBy('descr', 'asc')->lists('descr', 'id');
		$brands     = [''     => 'Select Brand'] + Brand::orderBy('descr', 'asc')->lists('descr', 'id');

		return View::make('product.create')
			->with(compact('categories'))
			->with(compact('brands'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$rules = [
			'category_id'  => 'required|integer',
			'brand_id'     => 'required|integer',
			'modeltitle'   => 'required|max:255',
			'referenceid'  => 'required|max:255',
			'price'        => 'required|numeric',
			'sortorder'    => 'required|integer',
			'specialprice' => 'numeric',
			'remarks'      => 'max:2000',
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->passes())
		{
			$record = new Product;
			$record->category_id = Input::get('category_id');
			$record->brand_id = Input::get('brand_id');
			$record->modeltitle = Input::get('modeltitle');
			$record->referenceid = Input::get('referenceid');
			$record->price = Input::get('price');
			$record->specialprice = Input::get('specialprice') ?: null;
			$record->remarks = Input::get('remarks');
			$record->active = Input::get('active', '0');
			$record->sortorder = Input::get('sortorder');
			try
			{
				$record->save();
			}
			catch (\Exception $e)
			{
				return \Redirect::back()->withInput()->withErrors($e->getMessage());
			}

			return \Redirect::route('product.index');
		}

		return Redirect::back()->withInput()->withErrors($validator);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function edit($id)
	{
		$record     = Product::findOrFail($id);
		$categories = ['' => 'Select Category'] + Category::orderBy('descr', 'asc')->lists('descr', 'id');
		$brands     = [''     => 'Select Brand'] + Brand::orderBy('descr', 'asc')->lists('descr', 'id');

		return View::make('product.edit')
			->with(compact('categories'))
			->with(compact('brands'))
			->with(compact('record'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function update($id)
	{

		$rules = [
			'category_id'  => 'required|integer',
			'brand_id'     => 'required|integer',
			'modeltitle'   => 'required|max:255',
			'referenceid'  => 'required|max:255',
			'price'        => 'required|numeric',
			'sortorder'    => 'required|integer',
			'specialprice' => 'numeric',
			'remarks'      => 'max:2000',
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->passes())
		{
			$record = Product::findOrFail($id);
			$record->category_id = Input::get('category_id');
			$record->brand_id = Input::get('brand_id');
			$record->modeltitle = Input::get('modeltitle');
			$record->referenceid = Input::get('referenceid');
			$record->price = Input::get('price');
			$record->specialprice = Input::get('specialprice') ?: null;
			$record->remarks = Input::get('remarks');
			$record->active = Input::get('active', '0');
			$record->sortorder = Input::get('sortorder');
			try
			{
				$record->save();
			}
			catch (\Exception $e)
			{
				return \Redirect::back()->withInput()->withErrors($e->getMessage());
			}

			return \Redirect::route('product.index');
		}

		return Redirect::back()->withInput()->withErrors($validator);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$record = Product::findOrFail($id);
		try
		{
			$record->delete();
		}
		catch (\Exception $e)
		{
			return \Redirect::back()->withInput()->withErrors($e->getMessage());
		}

		return \Redirect::route('product.index');
	}
}
