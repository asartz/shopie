<?php

class CategoryController extends BaseController {
	/**
	 * Make sure we are secure at row level
	 */
	public function __construct()
	{
		$this->beforeFilter(function($route, $request)
		{
			if (count(Category::find($route->getParameter('category', 0))) === 0)
			{
				return Redirect::route('forbidden');
			}
		}, ['only' => ['show', 'edit', 'update', 'destroy']]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$records = Category::orderBy('id', 'desc')->get();

		return View::make('category.index')->with(compact('records'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		return View::make('category.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$rules = [
			'descr'     => 'required|min:3|max:255',
			'sortorder' => 'required|integer',
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->passes())
		{
			$record = new Category;
			$record->descr = Input::get('descr');
			$record->active = Input::get('active', '0');
			$record->sortorder = Input::get('sortorder');
			try
			{
				$record->save();
			}
			catch (\Exception $e)
			{
				return \Redirect::back()->withInput()->withErrors($e->getMessage());
			}

			return \Redirect::route('category.index');
		}

		return Redirect::back()->withInput()->withErrors($validator);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function edit($id)
	{
		$record = Category::findOrFail($id);
		return View::make('category.edit')->with(compact('record'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function update($id)
	{

		$rules = [
			'descr'     => 'required|min:3|max:255',
			'sortorder' => 'required|integer',
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->passes())
		{
			$record = Category::findOrFail($id);
			$record->descr = Input::get('descr');
			$record->active = Input::get('active', '0');
			$record->sortorder = Input::get('sortorder');
			try
			{
				$record->save();
			}
			catch (\Exception $e)
			{
				return \Redirect::back()->withInput()->withErrors($e->getMessage());
			}

			return \Redirect::route('category.index');
		}

		return Redirect::back()->withInput()->withErrors($validator);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$record = Category::findOrFail($id);
		try
		{
			$record->delete();
		}
		catch (\Exception $e)
		{
			return \Redirect::back()->withInput()->withErrors($e->getMessage());
		}

		return \Redirect::route('category.index');
	}
}
