<?php

class AuthController extends BaseController {

	/**
	 * Show the application login form.
	 *
	 * @return Response
	 */
	public function getLogin()
	{
		return View::make('login');
	}

	/**
	 * Handle a login request to the application.
	 *
	 * @param  LoginRequest  $request
	 * @return Response
	 */
	public function postLogin()
	{
		$userdata = array(
			'username' => Input::get('username'),
			'password' => Input::get('password'),
			'active'   => 1,
		);

		if (Auth::attempt($userdata, (bool) Input::get('remember', 0)))
		{
			try
			{
				$record = new Login;
				$record->user_id = Auth::user()->id;
				$record->sessionid = Session::getId();
				$record->created_ip = Request::getClientIp();

				$record->save();
			}
			catch (\Exception $e)
			{
				return \Redirect::back()->withInput()->withErrors($e->getMessage());
			}

			return Redirect::intended('/');
		}

		return Redirect::route('login')->withErrors([
			'username' => 'Τα στοιχεία σύνδεσης δεν είναι σωστά.',
		]);
	}

	/**
	 * Log the user out of the application.
	 *
	 * @return Response
	 */
	public function getLogout()
	{
		$login = Login::where('user_id', Auth::user()->id)
			->where('sessionid', Session::getId())
			->whereNull('logout_at')
			->orderBy('id', 'desc')
			->first();

		if (count($login) === 1)
		{
			$login->logout_at = \Carbon\Carbon::now();
			$login->updated_ip = Request::getClientIp();

			$login->save();
		}

		Auth::logout();

		return Redirect::to('/');
	}

	/**
	 * Display forbidden message.
	 *
	 * @return Response
	 */
	public function forbidden()
	{
		return View::make('user.forbidden');
	}

}
