<?php

class HomeController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |    Route::get('/', 'HomeController@showWelcome');
    |
     */

    public function index()
    {
        $categories = [];
        $brands = [];
        $records = [];

        if (Input::has('category_id'))
        {
            $categories = Category::where('id', Input::get('category_id'))->get();
        }
        else
        {
            $categories = Category::orderBy('descr', 'asc')->get();
        }

        if (Input::has('brand_id'))
        {
            $brands = Brand::where('id', Input::get('brand_id'))->get();
        }
        else
        {
            $brands = Brand::orderBy('descr', 'asc')->get();
        }

        $records = Product::with('category')->with('brand');

        if (Input::has('category_id'))
        {
            $records = $records->where('category_id', '=', Input::get('category_id'));
        }

        if (Input::has('brand_id'))
        {
            $records = $records->where('brand_id', '=', Input::get('brand_id'));
        }

        // fts section. beware of sql injection. use parameters
        if (Input::has('fts'))
        {
            $records = $records->whereRaw('plainto_tsquery (?) @@ products.fts', [Input::get('fts')]);
        }

        $records = $records->where('active', '1')
            ->orderBy('id', 'desc')
            ->paginate(12);

        return View::make('index')
            ->with(compact('categories'))
            ->with(compact('brands'))
            ->with(compact('records'))
            ->with('itemsperrow', 3);
    }

    public function preview($id)
    {
        $record = Product::with('category')
            ->with('brand')
            ->where('slug', $id)
            ->first();

        $otheritems = Product::with('category')->with('brand')->where('slug', '<>', $id)->orderBy('id', 'desc')->take(3)->get();

        return View::make('product.preview')
            ->with(compact('categories'))
            ->with(compact('brands'))
            ->with(compact('record'))
            ->with(compact('otheritems'));
    }

    public function contact()
    {
        return View::make('contact');
    }

    /**
     * Handle contact form submission.
     *
     * @return Response
     */
    public function contactrequest()
    {
        $rules = [
            'fname' => 'required|min:2|max:50',
            'lname' => 'required|min:2|max:50',
            'email' => 'required|email',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {
            try
            {
                Session::flash('message', 'Thank you for your interest. A representative will contact you as soon as possible.');
                // TODO log message and notify us by email logic goes here
            }
            catch (\Exception $e)
            {
                return \Redirect::back()->withInput()->withErrors($e->getMessage());
            }

            return \Redirect::to('/');
        }

        return Redirect::back()->withInput()->withErrors($validator);
    }
}
