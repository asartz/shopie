<?php

class CartController extends BaseController {

/*
|--------------------------------------------------------------------------
| Cart Controller
|--------------------------------------------------------------------------
|
| This controller is responsible for Cart actions (add item to cart, remove item from cart etc)
|
 */

    public function cartadd($id)
    {
        $record = Product::with('category')
            ->with('brand')
            ->where('slug', $id)
            ->first();

        $otheritems = Product::with('category')->with('brand')->where('slug', '<>', $id)->orderBy('id', 'desc')->take(3)->get();

        /**
         * Add a row to the cart
         *
         * @param string|Array $id      Unique ID of the item|Item formated as array|Array of items
         * @param string       $name    Name of the item
         * @param int          $qty     Item qty to add to the cart
         * @param float        $price   Price of one item
         * @param Array        $options Array of additional options, such as 'size' or 'color'
         */
        Cart::add(array('id' => $record->id, 'name' => $record->modeltitle, 'qty' => 1, 'price' => (empty($record->specialprice)) ? $record->price : $record->specialprice));

        return View::make('cart.cartadd')
            ->with(compact('categories'))
            ->with(compact('brands'))
            ->with(compact('record'))
            ->with(compact('otheritems'));
    }

    public function cartremove($id)
    {
        /**
         * Remove a row from the cart
         *
         * @param  string  $id The id of the item
         * @return boolean
         */
        try
        {
            Cart::remove($id);
        }
        catch (\Exception $e)
        {
            return \Redirect::back()->withInput()->withErrors($e->getMessage());
        }

        return Redirect::back();
    }

    public function cartupdate($id, $qty)
    {
        /**
         * Update the quantity of one row of the cart
         *
         * @param  string        $id       The id of the item you want to update
         * @param  integer|Array $attribute   New quantity of the item|Array of attributes to update
         * @return boolean
         */

        Cart::update($id, $qty);

        return Redirect::back();
    }

    public function cartcheckout()
    {
        /**
         * Get the cart content
         *
         * @return CartCollection
         */
        $records = Cart::content();

        return View::make('cart.cart')
            ->with(compact('records'));
    }

    public function cartcouponadd($coupon = '')
    {
        $coupon = Coupon::where('descr', $coupon)
            ->first();

        if (count($coupon) === 1)
        {
            Session::put('coupon', $coupon);

            return Redirect::back();
        }
        else
        {
            return Redirect::back();
        }
    }

    public function cartcouponremove($coupon = '')
    {
        if (Session::has('coupon'))
        {
            Session::forget('coupon');

            return Redirect::back();
        }
        else
        {
            return Redirect::back();
        }
    }

    public function cartregister()
    {
        return View::make('cart.register');
    }

}