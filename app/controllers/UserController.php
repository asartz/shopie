<?php

class UserController extends BaseController {

    /**
     * Display no access / forbidden message when user tries to access a route he/she is not permitted to.
     *
     * @return Response
     */
    public function forbidden()
    {
        return View::make('user.forbidden');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function adminuserlist()
    {
        $records = User::where('usertype_id', '<>', 10)
            ->orderBy('usertype_id', 'asc')
            ->orderBy('fullname', 'asc')
            ->with('usertype')
            ->get();

        return View::make('adminuserlist.index')
            ->with(compact('records'));
    }

    /**
     * Implements login as a different user.
     * Caution: Only admin must have access to this route / controller action.
     *
     * @return Response
     */
    public function loginusingid($id)
    {
        if (Auth::check())
        {
            if (Auth::user()->usertype_id == 10)
            {
                Auth::logout();
                $user = User::findOrFail($id);
                Auth::login($user);
                return Redirect::to('/');
            }
        }
        else
        {
            return View::make('login');
        }
    }

    /**
     * Sign up a new user.
     *
     * @return Response
     */
    public function signup()
    {
        $rules = [
            'fullname' => 'required|min:3|max:150',
            'username' => 'required|min:3|max:30|alpha_dash|unique:users,username',
            'password' => 'required|min:3|max:150|same:retypepassword',
            'retypepassword' => 'same:password',
            'email' => 'required|email|unique:users,email',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {
            $record = new User;
            $record->username = Input::get('username');
            $record->password = \Hash::make(Input::get('password'));
            $record->email = Input::get('email');
            $record->fullname = Input::get('fullname');
            $record->usertype_id = 30;
            $record->active = 1;
            try
            {
                $record->save();
            }
            catch (\Exception $e)
            {
                return \Redirect::back()->withInput()->withErrors($e->getMessage());
            }

            $insertedId = $record->id;
            $user = User::findOrFail($insertedId);

            // manually login newly created user
            Auth::login($user);

            // now lets get the shipping address
            return \Redirect::route('shippingaddress');
        }

        return Redirect::back()->withInput()->withErrors($validator);
    }

    /**
     * Display shipping & billing address .
     *
     * @return Response
     */
    public function shippingaddress()
    {
        /**
         * Get the cart content
         *
         * @return CartCollection
         */
        $records = Cart::content();

        if (Auth::check())
        {
            $shippingaddress = Auth::user()->shippingaddress;

            $billingaddress = Auth::user()->billingaddress;
        }

        return View::make('cart.shipping')
            ->with(compact('records'))
            ->with(compact('shippingaddress'))
            ->with(compact('billingaddress'));
    }

    /**
     * Save shipping and billing address.
     *
     * @return Response
     */
    public function postshippingaddress()
    {
        $rules = [
            'fullname' => 'required|min:3|max:255',
            'streetaddress' => 'required|min:3|max:255',
            'streetno' => 'required|max:30',
            'region' => 'required|max:255',
            'city' => 'required|max:255',
            'zipcode' => 'required|max:30',
            'phone' => 'required|min:3|max:30',
        ];

        // if billing is not same as shipping we have got to validate billing form fields
        if (!Input::has('chksameasbilling'))
        {
            $rules['billfullname'] = 'required|min:3|max:255';
            $rules['billstreetaddress'] = 'required|min:3|max:255';
            $rules['billstreetno'] = 'required|max:30';
            $rules['billregion'] = 'required|max:255';
            $rules['billcity'] = 'required|max:255';
            $rules['billzipcode'] = 'required|max:30';
            $rules['billphone'] = 'required|min:3|max:30';
        }

        $validator = Validator::make(Input::all(), $rules);

        $arrShippingAddress = ['user_id' => Auth::user()->id, 'fullname' => Input::get('fullname'), 'streetaddress' => Input::get('streetaddress'), 'streetno' => Input::get('streetno'), 'region' => Input::get('region'), 'city' => Input::get('city'), 'zipcode' => Input::get('zipcode'), 'phone' => Input::get('phone'), 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()];

        if (Input::has('chksameasbilling'))
        {
            $arrBillingAddress = $arrShippingAddress;
        }
        else
        {
            $arrBillingAddress = ['user_id' => Auth::user()->id, 'fullname' => Input::get('billfullname'), 'streetaddress' => Input::get('billstreetaddress'), 'streetno' => Input::get('billstreetno'), 'region' => Input::get('billregion'), 'city' => Input::get('billcity'), 'zipcode' => Input::get('billzipcode'), 'phone' => Input::get('billphone'), 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()];
        }

        if ($validator->passes())
        {
            try {
                // if billing & shipping address do not exist then insert them, else update existing data
                if (count(Auth::user()->shippingaddress) === 0)
                {
                    DB::transaction(function () use ($arrShippingAddress, $arrBillingAddress)
                    {
                        DB::table('shippingaddresses')->insert($arrShippingAddress);

                        DB::table('billingaddresses')->insert($arrBillingAddress);
                    });
                }
                else
                {
                    DB::transaction(function () use ($arrShippingAddress, $arrBillingAddress)
                    {
                        DB::table('shippingaddresses')->where('user_id', Auth::user()->id)->update(array_except($arrShippingAddress, 'user_id'));

                        DB::table('billingaddresses')->where('user_id', Auth::user()->id)->update(array_except($arrBillingAddress, 'user_id'));
                    });
                }
            }
            catch (Exception $e)
            {
                return \Redirect::back()->withInput()->withErrors($e->getMessage());
            }

            return \Redirect::route('placeorder');
        }

        return Redirect::back()->withInput()->withErrors($validator);
    }

    /**
     * Display a listing of Auth user orders.
     *
     * @return Response
     */
    public function orders()
    {
        $records = Auth::user()->orders()->with('user')->with('coupon')->with('orderdetails')->orderBy('id', 'desc')->get();

        return View::make('order.index')
            ->with(compact('records'));
    }

    /**
     * Get user profile.
     *
     * @return Response
     */
    public function profile()
    {

        if (Auth::check())
        {
            $shippingaddress = Auth::user()->shippingaddress;

            $billingaddress = Auth::user()->billingaddress;
        }

        return View::make('user.profile')
            ->with(compact('shippingaddress'))
            ->with(compact('billingaddress'));
    }

    /**
     * Save user profile.
     *
     * @return Response
     */
    public function postprofile()
    {
        $rules = [
            'password' => 'min:3|max:150|same:retypepassword',
            'retypepassword' => 'same:password',
            'email' => 'required|email',

            'fullname' => 'required|min:3|max:255',
            'streetaddress' => 'required|min:3|max:255',
            'streetno' => 'required|max:30',
            'region' => 'required|max:255',
            'city' => 'required|max:255',
            'zipcode' => 'required|max:30',
            'phone' => 'required|min:3|max:30',

            'billfullname' => 'required|min:3|max:255',
            'billstreetaddress' => 'required|min:3|max:255',
            'billstreetno' => 'required|max:30',
            'billregion' => 'required|max:255',
            'billcity' => 'required|max:255',
            'billzipcode' => 'required|max:30',
            'billphone' => 'required|min:3|max:30',
        ];

        $validator = Validator::make(Input::all(), $rules);

        $arrUser = ['email' => Input::get('email'), 'updated_at' => \Carbon\Carbon::now()];
        // change the password only if we need to
        if (!empty(Input::get('password')))
        {
            $arrUser['password'] = \Hash::make(Input::get('password'));
        }

        $arrShippingAddress = ['user_id' => Auth::user()->id, 'fullname' => Input::get('fullname'), 'streetaddress' => Input::get('streetaddress'), 'streetno' => Input::get('streetno'), 'region' => Input::get('region'), 'city' => Input::get('city'), 'zipcode' => Input::get('zipcode'), 'phone' => Input::get('phone'), 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()];

        $arrBillingAddress = ['user_id' => Auth::user()->id, 'fullname' => Input::get('billfullname'), 'streetaddress' => Input::get('billstreetaddress'), 'streetno' => Input::get('billstreetno'), 'region' => Input::get('billregion'), 'city' => Input::get('billcity'), 'zipcode' => Input::get('billzipcode'), 'phone' => Input::get('billphone'), 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()];

        if ($validator->passes())
        {
            try {
                // if billing & shipping address do not exist then insert them, else update existing data
                if (count(Auth::user()->shippingaddress) === 0)
                {
                    DB::transaction(function () use ($arrUser, $arrShippingAddress, $arrBillingAddress)
                    {
                        DB::table('users')->where('id', Auth::user()->id)->update($arrUser);

                        DB::table('shippingaddresses')->insert($arrShippingAddress);

                        DB::table('billingaddresses')->insert($arrBillingAddress);
                    });
                }
                else
                {
                    DB::transaction(function () use ($arrUser, $arrShippingAddress, $arrBillingAddress)
                    {
                        DB::table('users')->where('id', Auth::user()->id)->update($arrUser);

                        DB::table('shippingaddresses')->where('user_id', Auth::user()->id)->update(array_except($arrShippingAddress, ['user_id', 'created_at']));

                        DB::table('billingaddresses')->where('user_id', Auth::user()->id)->update(array_except($arrBillingAddress, ['user_id', 'created_at']));
                    });
                }
            }
            catch (Exception $e)
            {
                return \Redirect::back()->withInput()->withErrors($e->getMessage());
            }

            return Redirect::to('/');
        }

        return Redirect::back()->withInput()->withErrors($validator);
    }

    /**
     * Place order.
     *
     * @return Response
     */
    public function placeorder()
    {
        /**
         * Get the cart content
         *
         * @return CartCollection
         */
        $records = Cart::content();

        if (Auth::check())
        {
            $shippingaddress = Auth::user()->shippingaddress;

            $billingaddress = Auth::user()->billingaddress;
        }

        return View::make('cart.placeorder')
            ->with(compact('records'))
            ->with(compact('shippingaddress'))
            ->with(compact('billingaddress'));
    }

    /**
     * Save order.
     *
     * @return Response
     */
    public function postplaceorder()
    {
        $rules = [];

        $validator = Validator::make(Input::all(), $rules);

        $arrOrder = [
            'user_id' => Auth::user()->id,
            'coupon_id' => (!Session::has('coupon')) ? null : Session::get('coupon')->id,
            'cartvalue' => Cart::total(),
            'discountvalue' => (!Session::has('coupon')) ? null : Session::get('coupon')->discountprice,
            'totalcost' => (!Session::has('coupon')) ? Cart::total() : Cart::total() - Session::get('coupon')->discountprice,
            'datecreated' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ];

        $arrOrderdetails = [];

        /**
         * Get the cart content
         *
         * @return CartCollection
         */
        $records = Cart::content();

        foreach ($records as $record)
        {
            array_push($arrOrderdetails, ['order_id' => '1', 'product_id' => $record->id, 'quantity' => $record->qty, 'price' => $record->price, 'rowcost' => $record->subtotal, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);
        }

        if ($validator->passes())
        {
            try {

                DB::transaction(function () use ($arrOrder, $arrOrderdetails)
                {
                    $order_id = DB::table('orders')->insertGetId($arrOrder);

                    foreach ($arrOrderdetails as $key => &$value)
                    {
                        $value['order_id'] = $order_id;
                    }

                    DB::table('orderdetails')->insert($arrOrderdetails);
                });

            }
            catch (Exception $e)
            {
                return \Redirect::back()->withInput()->withErrors($e->getMessage());
            }

            /**
             * Empty the cart
             *
             * @return boolean
             */
            Cart::destroy();

            Session::flash('message', 'Your order was successfully placed. Thank you.');

            return \Redirect::route('myorders');
        }

        return Redirect::back()->withInput()->withErrors($validator);
    }
}
