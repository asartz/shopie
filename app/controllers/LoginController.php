<?php

class LoginController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$rules = [

			'dtin' => 'date_format:d/m/Y',
			'dtto' => 'date_format:d/m/Y',
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->passes())
		{

			$users   = [''   => 'Select User'] + User::orderBy('username', 'asc')->lists('username', 'id');
			$records = Login::orderBy('id', 'desc')->with('user');

			if (Input::has('dtin'))
			{
				$records = $records->where('created_at', '>=', Input::get('dtin').' 00:00:00');
			}

			if (Input::has('dtto'))
			{
				$records = $records->where('created_at', '<=', Input::get('dtto').' 23:59:59');
			}

			if (Input::has('user_id'))
			{
				$records = $records->where('user_id', '=', Input::get('user_id'));
			}

			$records = $records->paginate(50);

			return View::make('loginparam.index')
				->with(compact('records'))
				->with(compact('users'));
		}

		return Redirect::back()->withInput()->withErrors($validator);

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
}
