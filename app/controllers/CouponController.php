<?php

class CouponController extends BaseController {
	/**
	 * Make sure we are secure at row level
	 */
	public function __construct()
	{
		$this->beforeFilter(function($route, $request)
		{
			if (count(Coupon::find($route->getParameter('coupon', 0))) === 0)
			{
				return Redirect::route('forbidden');
			}
		}, ['only' => ['show', 'edit', 'update', 'destroy']]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$records = Coupon::orderBy('id', 'desc')->get();

		return View::make('coupon.index')->with(compact('records'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		return View::make('coupon.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$rules = [
			'descr' => 'required|min:3|max:255',
			'validdatefrom' => 'required|date_format:d/m/Y',
			'validdateto' => 'required|date_format:d/m/Y',
			'discountprice' => 'required|numeric',
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->passes())
		{
			$record = new Coupon;
			$record->descr = Input::get('descr');
			$record->validdatefrom = Input::get('validdatefrom');
			$record->validdateto = Input::get('validdateto');
			$record->discountprice = Input::get('discountprice');
			$record->active = Input::get('active', '0');
			try
			{
				$record->save();
			}
			catch (\Exception $e)
			{
				return \Redirect::back()->withInput()->withErrors($e->getMessage());
			}

			return \Redirect::route('coupon.index');
		}

		return Redirect::back()->withInput()->withErrors($validator);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function edit($id)
	{
		$record = Coupon::findOrFail($id);
		return View::make('coupon.edit')->with(compact('record'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function update($id)
	{

		$rules = [
			'descr' => 'required|min:3|max:255',
			'validdatefrom' => 'required|date_format:d/m/Y',
			'validdateto' => 'required|date_format:d/m/Y',
			'discountprice' => 'required|numeric',
		];

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->passes())
		{
			$record = Coupon::findOrFail($id);
			$record->descr = Input::get('descr');
			$record->validdatefrom = Input::get('validdatefrom');
			$record->validdateto = Input::get('validdateto');
			$record->discountprice = Input::get('discountprice');
			$record->active = Input::get('active', '0');
			try
			{
				$record->save();
			}
			catch (\Exception $e)
			{
				return \Redirect::back()->withInput()->withErrors($e->getMessage());
			}

			return \Redirect::route('coupon.index');
		}

		return Redirect::back()->withInput()->withErrors($validator);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$record = Coupon::findOrFail($id);
		try
		{
			$record->delete();
		}
		catch (\Exception $e)
		{
			return \Redirect::back()->withInput()->withErrors($e->getMessage());
		}

		return \Redirect::route('coupon.index');
	}
}
