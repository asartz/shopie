@extends('layouts.master')

@section('title')
        <title>Sign In - {{Config::get('shopie.CLIENT_FIRM')}}</title>
@endsection

@section('meta')
        <meta content="Sign In to shopie" name="description" />
@endsection

@section('content')
  <!-- login -->
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Sign In</h3>
                </div>
                <div class="panel-body">
                    {{ Form::open( array('route' => array('authenticate'), 'role' => 'form') ) }}
                    <fieldset>
                        <div class="form-group">
                            {{ Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'Username']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) }}
                        </div>
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('remember', 0, null) }} Remember me
                            </label>
                        </div>
                        {{ Form::submit('Login', ['class' => 'btn btn-md btn-success btn-block']) }}
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop