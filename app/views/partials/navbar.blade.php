<!-- Fixed navbar -->
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ URL::to('/') }}">SHOPIE</a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li><a href="{{URL::route('contact')}}">Contact</a></li>
        <li><a href="{{URL::route('cartcheckout')}}">My Cart ({{Cart::count();}})</a></li>
        @if (Auth::check() and in_array(Auth::user()->usertype_id, [10,20,30]))
          <li><a href="{{ URL::route('myorders') }}">My Orders ({{Auth::user()->orders->count();}})</a></li>
          @if (Auth::check() and in_array(Auth::user()->usertype_id, [10,20]))
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Setup <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li class="dropdown-header"><h4>Inventory</h4></li>
              <li><a href="{{ URL::route('product.index') }}">Products</a></li>
              <li class="divider"></li>
              <li class="dropdown-header"><h4>Parameters</h4></li>
              <li><a href="{{URL::route('category.index')}}">Categories</a></li>
              <li><a href="{{URL::route('brand.index')}}">Brands</a></li>
              <li><a href="{{ URL::route('coupon.index') }}">Coupons</a></li>
              @if (Auth::user()->usertype_id === 10)
              <li class="divider"></li>
              <li class="dropdown-header"><h4>Security</h4></li>
              <li><a href="{{URL::route('adminuserlist')}}">Users</a></li>
              @endif
              <li class="divider"></li>
              <li class="dropdown-header"><h4>Statistics</h4></li>
              <li><a href="{{URL::route('loginparam.index')}}">Logins</a></li>
            </ul>
          </li>
          @endif
        @endif
      </ul>
      <ul class="nav navbar-nav navbar-right" id="navbar-login">
        @if (Auth::check())
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->username }} <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            @if (Auth::check() and in_array(Auth::user()->usertype_id, [10,20,30]))
              <li><a href="{{URL::route('profile')}}">Profile</a></li>
            @endif
            <li><a href="{{URL::route('logout')}}">Sign Out</a></li>
          </ul>
        </li>
        @else
        <li>
          <a href="{{ URL::route('login') }}">Sign In</a>
        </li>
        @endif
      </ul>

      @if (Route::is('index'))
      {{ Form::open( ['route' => ['index'], 'method' => 'GET', 'files' => false, 'class' => 'navbar-form navbar-right', 'role' => 'search'] ) }}
      <div class="input-group" id="navbar-search">
        {{ Form::text('fts', Input::get('fts', null), ['class' => 'form-control', 'placeholder' => 'Search']) }}
        <div class="input-group-btn">
          <button class="btn btn-success" type="submit"><i class="glyphicon glyphicon-search"></i></button>
        </div>
      </div>
      {{ Form::close() }}
      @endif

    </div><!--/.nav-collapse -->
  </div>
</div>
