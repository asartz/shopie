{{ Form::open(array('method'=>'DELETE', 'route' => array($destroyRoute, $destroyId))) }}
{{ Form::submit('Delete Record', ['class' => 'btn btn-danger btn-xs tooltips',  'data-placement' => 'right', 'data-original-title' => 'Record will be deleted immediately!', 'id' => 'btn-delete_' . $destroyId, 'disabled']) }}
{{ Form::checkbox('delete', 0, false, ['onclick' => 'if ($(this).is(\':checked\')) { $(\'#btn-delete_' . $destroyId . '\').removeAttr(\'disabled\'); } else { $(\'#btn-delete_' . $destroyId . '\').attr(\'disabled\', \'disabled\'); } ']) }}
{{ Form::close() }}
