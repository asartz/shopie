@if (Session::has('message'))
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-info">
				<a class="close" data-dismiss="alert" href="#"></a>
				<ul>
					<li>{{ Session::get('message') }}</li>
				</ul>
			</div>
		</div>
	</div>
@endif

@if ($errors->any())
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-danger">
				<a class="close" data-dismiss="alert" href="#"></a>
				<ul>
					{{ implode('', $errors->all('<li>:message</li>')) }}
				</ul>
			</div>
		</div>
	</div>
@endif