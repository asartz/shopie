@extends('layouts.master')

@section('title')
    <title>Contact info - {{Config::get('shopie.CLIENT_FIRM')}}</title>
@endsection

@section('meta')
    <meta content="shopie, contact" name="keywords" />
    <meta content="Shopie contact page" name="description" />
@endsection

@section('styles')
    <style>
        .map {
            min-width: 300px;
            min-height: 300px;
            width: 100%;
            height: 100%;
        }

        .header {
            background-color: #F5F5F5;
            color: #36A0FF;
            height: 70px;
            font-size: 27px;
            padding: 10px;
        }
    </style>
@endsection

@section('scripts')
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>

    <script type="text/javascript">
        jQuery(function ($) {
            function init_map1() {
                var myLocation = new google.maps.LatLng(37.92016, 23.72792);
                var mapOptions = {
                    center: myLocation,
                    zoom: 16
                };
                var marker = new google.maps.Marker({
                    position: myLocation,
                    title: "Property Location"
                });
                var map = new google.maps.Map(document.getElementById("map1"),
                    mapOptions);
                marker.setMap(map);
            }
            init_map1();
        });
    </script>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="well well-sm">
                {{ Form::open( ['route' => ['contactrequest'], 'class'=>'form-horizontal', 'role' => 'form'] ) }}
                    <fieldset>
                        <legend class="text-center header">Contact us</legend>
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-1">
                                {{ Form::text('fname', null, ['class' => 'form-control', 'placeholder' => 'First Name *']) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-1">
                                 {{ Form::text('lname', null, ['class' => 'form-control', 'placeholder' => 'Last Name *']) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-1">
                                 {{ Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email *']) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-1">
                                 {{ Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Phone']) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-1">
                                 {{ Form::textarea('message', null, ['class' => 'form-control', 'placeholder' => 'Enter your massage for us here. We will get back to you within 2 business days.', 'rows' => '7']) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary btn-lg">Submit</button>
                            </div>
                        </div>
                    </fieldset>
                {{ Form::close() }}
                <small>fields marked with an * are required</small>
            </div>
        </div>
        <div class="col-md-6">
            <div>
                <div class="panel panel-default">
                    <div class="text-center header">Our Office</div>
                    <div class="panel-body text-center">
                        <h4>Address</h4>
                        <div>
                        20 Kakavias Street<br />
                        Athens, Agios Dimitrios<br />
                        #302109821788<br />
                        shopie@versus-software.gr<br />
                        </div>
                        <hr />
                        <div id="map1" class="map">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection