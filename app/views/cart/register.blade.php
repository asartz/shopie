@extends('layouts.master')

@section('title')
        <title>Sign In or Register - {{Config::get('shopie.CLIENT_FIRM')}}</title>
@endsection

@section('meta')
    <meta content="sign in register" name="keywords" />
    <meta content="sign in register" name="description" />
@endsection

<style>
   .form-control { margin-bottom: 10px; }
</style>

@section('content')
 <!-- Page Header -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Sign In or Sign Up
        </h1>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-5 well well-sm">
            <legend><a href="#"><i class="fa fa-globe"></i></a> I already have an account</legend>
            {{ Form::open( array('route' => array('authenticate'), 'role' => 'form') ) }}
                <input class="form-control" name="username" placeholder="Username" type="text" />
                <input class="form-control" name="password" placeholder="Password" type="password" />
                <br />
                <br />
                {{ Form::submit('Sign In', ['class' => 'btn btn-md btn-success btn-block']) }}
            {{ Form::close() }}
        </div>

        <div class="col-md-5 col-md-offset-1 well well-sm">
            <legend><a href="#"><i class="fa fa-globe"></i></a> I want to create an account</legend>
            {{ Form::open( array('route' => array('signup'), 'role' => 'form') ) }}
                {{ Form::text('fullname', null, ['class' => 'form-control', 'placeholder' => 'Full name *']) }}
                {{ Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email *']) }}
                {{ Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'Username *']) }}
                {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password *']) }}
                {{ Form::password('retypepassword', ['class' => 'form-control', 'placeholder' => 'Retype password *']) }}
                <br />
                <br />
                {{ Form::submit('Sign Up', ['class' => 'btn btn-md btn-primary btn-block']) }}
            {{ Form::close() }}
        </div>
    </div>
</div>

@stop