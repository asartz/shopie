@extends('layouts.master')

@section('title')
        <title>Add to Cart - {{Config::get('shopie.CLIENT_FIRM')}}</title>
@endsection

@section('meta')
    <meta content="product, add to cart" name="keywords" />
    <meta content="{{$record->modeltitle}}" name="description" />
@endsection

@section('headscripts')

@stop

@section('content')
 <!-- Page Header -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Add to Cart
        </h1>
    </div>
</div>
<div class="row">
    <div class="container">
        <table id="cart" class="table table-hover table-condensed">
            <thead>
                <tr>
                    <th style="width:80%">Product</th>
                    <th style="width:10%">Price</th>
                    <th style="width:10%"></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td data-th="Product">
                        <div class="row">
                            <div class="col-sm-2 hidden-xs"><img src="http://placehold.it/100x100" alt="..." class="img-responsive"/></div>
                            <div class="col-sm-10">
                                <h4 class="nomargin">{{ $record->brand->descr }} - {{$record->modeltitle}}</h4>
                                <p>{{$record->remarks}}</p>
                            </div>
                        </div>
                    </td>
                    <td data-th="Price">$
                        @if (empty($record->specialprice))
                            <b>{{$record->price}}</b>
                        @else
                            <strike>{{$record->price}}</strike> <b>{{$record->specialprice}}</b>
                        @endif
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr class="visible-xs">
                    <td class="text-center"><strong>Total
                        @if (empty($record->specialprice))
                            <b>{{$record->price}}</b>
                        @else
                            <b>{{$record->specialprice}}</b>
                        @endif
                    </strong></td>
                </tr>
                <tr>
                    <td><a href="{{URL::previous()}}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
                    <td colspan="2" class="hidden-xs"><strong>Total $
                        @if (empty($record->specialprice))
                            <b>{{$record->price}}</b>
                        @else
                            <b>{{$record->specialprice}}</b>
                        @endif
                    </strong></td>
                    <td class="hidden-xs text-center"></td>
                    <td><a href="{{URL::route('cartcheckout')}}" class="btn btn-success btn-block">View Cart <i class="fa fa-angle-right"></i></a></td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<div class="row" style="padding-top: 40px;">
    <h4>Other items</h4>

    <div class='col-md-12'>
        <div class="row">
            @foreach ($otheritems as $otheritem)
                <div class="col-md-4">
                    <a href="{{URL::route('preview', [$otheritem->slug])}}"><img alt="" src="http://placehold.it/150x150">
                        <div class="info">
                            <div class="price col-md-12">
                                <h5>
                                    {{$otheritem->brand->descr}}</h5>
                                <h5>
                                    {{$otheritem->modeltitle}}</h5>
                                <h5 class="price-text-color">
                                    <b>€</b>
                                    @if (empty($otheritem->specialprice))
                                        <b>{{$otheritem->price}}</b>
                                    @else
                                        <strike>{{$otheritem->price}}</strike> <b>{{$otheritem->specialprice}}</b>
                                    @endif</h5>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>

@stop