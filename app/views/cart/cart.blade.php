@extends('layouts.master')

@section('title')
        <title>My Cart - {{Config::get('shopie.CLIENT_FIRM')}}</title>
@endsection

@section('meta')
    <meta content="product, my cart" name="keywords" />
    <meta content="my cart" name="description" />
@endsection

@section('headscripts')

@stop

@section('content')
 <!-- Page Header -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">My Cart
        </h1>
    </div>
</div>
<div class="row">
    <div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-10 col-md-offset-1">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th class="text-center">Price</th>
                        <th class="text-center">Total</th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>

                @foreach ($records as $record)
                    <tr>
                        <td class="col-sm-8 col-md-6">
                        <div class="media">
                            <a class="pull-left" href="#"> <img class="media-object" src="http://placehold.it/100x100" style="width: 72px; height: 72px;"> </a>
                            <div class="media-body">
                                <h4 class="media-heading"><a href="#">{{$record->name}}</a></h4>
                                <h5 class="media-heading"> by <a href="#">Brand name</a></h5>
                                <span>Status: </span><span class="text-success"><strong>In Stock</strong></span>
                            </div>
                        </div></td>
                        <td class="col-sm-1 col-md-1" style="text-align: center">
                        <input type="number" class="form-control" id="qty_{{$record->rowid}}" value="{{$record->qty}}">
                        </td>
                        <td class="col-sm-1 col-md-1 text-center"><strong>${{$record->price}}</strong></td>
                        <td class="col-sm-1 col-md-1 text-center"><strong>${{number_format($record->price * $record->qty, 2, '.', '');}}</strong></td>
                        <td class="col-sm-1 col-md-1">
                            <a href="javascript:void(0);" role="button" class="fa fa-refresh btn btn-success" onclick="window.location.href='{{URL::to('/')}}/cart/update/{{$record->rowid}}/'+jQuery('#qty_{{$record->rowid}}').val();"></a>
                            <a href="{{URL::route('cartremove', [$record->rowid])}}" role="button" class="fa fa-trash btn btn-danger"></a>
                        </td>
                    </tr>
                @endforeach

                @if (Cart::count() > 0)
                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td><h5>Discount Coupon</h5></td>
                        <td>
                            @if ( !Session::has('coupon'))
                                <input type="text" class="form-control" id="coupon" value="" placeholder="Code">
                            @else
                                <input type="text" class="form-control" id="coupon" value="{{Session::get('coupon')->descr}}" placeholder="Code" disabled="true">
                            @endif
                        </td>
                        <td>
                            @if (!Session::has('coupon'))
                                <a href="javascript:void(0);" role="button" title="Recalculate Total Cost" class="fa fa-calculator btn btn-success" onclick="window.location.href='{{URL::to('/')}}/cart/coupon/add/'+jQuery('#coupon').val();"></a>
                            @else
                                <a href="javascript:void(0);" role="button" title="Remove Coupon" class="fa fa-remove btn btn-danger" onclick="window.location.href='{{URL::to('/')}}/cart/coupon/remove/'+jQuery('#coupon').val();"></a>
                            @endif
                        </td>
                    </tr>
                @endif
                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td>   </td>
                        <td><h5>Cart Total</h5></td>
                        <td class="text-right"><h5><strong>${{number_format(Cart::total(), 2, '.', '');}}</strong></h5></td>
                    </tr>

                    @if (Session::has('coupon'))
                        <tr>
                            <td>   </td>
                            <td>   </td>
                            <td>   </td>
                            <td><h5>Coupon Amount</h5></td>
                            <td class="text-right"><h5><strong>${{number_format(Session::get('coupon')->discountprice, 2, '.', '');}}</strong></h5></td>
                        </tr>
                    @endif
                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td>   </td>
                        <td><h3>Total</h3></td>
                        <td class="text-right"><h3><strong>
                            @if (!Session::has('coupon'))
                                ${{number_format(Cart::total(), 2, '.', '');}}
                            @else
                                ${{number_format(Cart::total() - Session::get('coupon')->discountprice, 2, '.', '');}}
                            @endif
                        </strong></h3></td>
                    </tr>
                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td>   </td>
                        <td>
                        <a href="{{ URL::to('/') }}" role="button" class="fa fa-shopping-cart btn btn-info"> Continue Shopping</a>
                        </td>
                        <td>
                        <a href="{{(!Auth::check()) ? URL::route('cartregister') : URL::route('shippingaddress')}}" role="button" class="fa fa-play btn btn-success" @if (Cart::count() === 0) disabled @endif> Checkout</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

</div>

@stop