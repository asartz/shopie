@extends('layouts.master')

@section('title')
        <title>Place Order - {{Config::get('shopie.CLIENT_FIRM')}}</title>
@endsection

@section('meta')
    <meta content="place order" name="keywords" />
    <meta content="place order" name="description" />
@endsection

<style>
   .form-control { margin-bottom: 10px; }
</style>

@section('content')
 <!-- Page Header -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Place Order
        </h1>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">
                        <div class="row">
                            <div class="col-md-6">
                                <h5><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    @foreach ($records as $record)
                        <div class="row">
                            <div class="col-md-2"><img class="img-responsive" src="http://placehold.it/100x70">
                            </div>
                            <div class="col-md-4">
                                <h4 class="product-name"><strong>{{$record->name}}</strong></h4><h4><small>Product description</small></h4>
                            </div>
                            <div class="col-md-6">
                                <h6><strong>$ {{number_format($record->price * $record->qty, 2, '.', '');}} <span class="text-muted">({{$record->qty}} Pcs)</span></strong></h6>
                            </div>
                        </div>
                        <hr>
                        @if (Session::has('coupon'))
                          <tr>
                              <h5 class="text-right">Coupon Amount: <strong>${{number_format(Session::get('coupon')->discountprice, 2, '.', '');}}</strong></h5>
                          </tr>
                        @endif
                    @endforeach
                </div>
                <div class="panel-footer">
                    <div class="row text-center">
                        <div class="col-xs-9">
                            <h4 class="text-right">Total: <strong>
                                @if (!Session::has('coupon'))
                                    ${{number_format(Cart::total(), 2, '.', '');}}
                                @else
                                    ${{number_format(Cart::total() - Session::get('coupon')->discountprice, 2, '.', '');}}
                                @endif
                            </strong></h4>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
        <legend><i class="fa fa-envelope"></i> Shipping Address</legend>
            {{ Form::open( array('route' => array('postplaceorder'), 'role' => 'form') ) }}
                <div class="form-group">
                  <div class="col-sm-12">
                    {{ Form::text('fullname',(!empty($shippingaddress)) ? $shippingaddress->fullname : null, ['class' => 'form-control', 'disabled' => 'true', 'placeholder' => 'Full name']) }}
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-12">
                    {{ Form::text('streetaddress',(!empty($shippingaddress)) ? $shippingaddress->streetaddress : null, ['class' => 'form-control', 'disabled' => 'true', 'placeholder' => 'Street Address']) }}
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-12">
                    {{ Form::text('streetno',(!empty($shippingaddress)) ? $shippingaddress->streetno : null, ['class' => 'form-control', 'disabled' => 'true', 'placeholder' => 'Street No']) }}
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-12">
                    {{ Form::text('region',(!empty($shippingaddress)) ? $shippingaddress->region : null, ['class' => 'form-control', 'disabled' => 'true', 'placeholder' => 'Region']) }}
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-12">
                    {{ Form::text('city',(!empty($shippingaddress)) ? $shippingaddress->city : null, ['class' => 'form-control', 'disabled' => 'true', 'placeholder' => 'City']) }}
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-12">
                    {{ Form::text('zipcode',(!empty($shippingaddress)) ? $shippingaddress->zipcode : null, ['class' => 'form-control', 'disabled' => 'true', 'placeholder' => 'Zipcode']) }}
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-12">
                    {{ Form::text('phone',(!empty($shippingaddress)) ? $shippingaddress->phone : null, ['class' => 'form-control', 'disabled' => 'true', 'placeholder' => 'Phone']) }}
                  </div>
                </div>

                <br />
                <legend><i class="fa fa-envelope"></i> Billing Address</legend>

                <div class="form-group">
                  <div class="col-sm-12">
                    {{ Form::text('billfullname',(!empty($billingaddress)) ? $billingaddress->fullname : null, ['class' => 'form-control', 'disabled' => 'true', 'placeholder' => 'Full name']) }}
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-12">
                    {{ Form::text('billstreetaddress',(!empty($billingaddress)) ? $billingaddress->streetaddress : null, ['class' => 'form-control', 'disabled' => 'true', 'placeholder' => 'Street Address']) }}
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-12">
                    {{ Form::text('billstreetno',(!empty($billingaddress)) ? $billingaddress->streetno : null, ['class' => 'form-control', 'disabled' => 'true', 'placeholder' => 'Street No']) }}
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-12">
                    {{ Form::text('billregion',(!empty($billingaddress)) ? $billingaddress->region : null, ['class' => 'form-control', 'disabled' => 'true', 'placeholder' => 'Region']) }}
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-12">
                    {{ Form::text('billcity',(!empty($billingaddress)) ? $billingaddress->city : null, ['class' => 'form-control', 'disabled' => 'true', 'placeholder' => 'City']) }}
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-12">
                    {{ Form::text('billzipcode',(!empty($billingaddress)) ? $billingaddress->zipcode : null, ['class' => 'form-control', 'disabled' => 'true', 'placeholder' => 'Zipcode']) }}
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-12">
                    {{ Form::text('billphone',(!empty($billingaddress)) ? $billingaddress->phone : null, ['class' => 'form-control', 'disabled' => 'true', 'placeholder' => 'Phone']) }}
                  </div>
                </div>

                <br />
                <br />
                {{ Form::submit('Place Order', ['class' => 'btn btn-md btn-success btn-block']) }}
            {{ Form::close() }}
        </div>
    </div>
</div>

@stop