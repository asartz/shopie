@extends('layouts.master')

@section('styles')
  {{ HTML::style('assets/custom/css/table.css') }}
@stop

@section('scripts')
  {{ HTML::script('assets/custom/js/table.js') }}
@stop

@section('content')
      <div class="row">
      <div class="col-md-12">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Users</h3>
            <div class="pull-right">
              <span class="clickable filter" data-toggle="tooltip" title="Toggle table filter" data-container="body">
                <i class="glyphicon glyphicon-filter"></i>
              </span>
            </div>
          </div>
          <div class="panel-body">
            <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter" />
          </div>
          <table class="table table-hover table-condensed" id="dev-table">
            <thead>
              <tr>
                <th>#</th>
                <th>Username</th>
                <th>Full name</th>
                <th>Email</th>
                <th>Phone</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @foreach ($records as $record)
                <tr>
                  <td> {{ $record->id }}</td>
                  <td> {{ $record->username }} </td>
                  <td> {{ $record->fullname }} </td>
                  <td> {{ $record->email }} </td>
                  <td> {{ $record->phone }} </td>
                  <td>
              <div class="dropdown pull-left">
                <button role="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">Links</button>
                <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
                  <li role="presentation">{{ HTML::linkRoute('loginusingid', 'Login as User Id [' . $record->id . ']', array('id' => $record->id), array('role' => 'menuitem', 'tabindex' => '-1')) }}</li>
                </ul>
              </div>
            </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
@stop