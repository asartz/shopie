@extends('layouts.master')

@section('styles')
  {{ HTML::style('assets/custom/css/table.css') }}
@stop

@section('scripts')
  {{ HTML::script('assets/custom/js/table.js') }}
@stop

@section('content')
      <div class="row">
      <div class="col-md-12">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Brands</h3>
            <div class="pull-right">
              <span class="clickable" data-toggle="tooltip" title="New Record" data-container="body">
                <a href="{{URL::route('brand.create')}}">
                  <i class="glyphicon glyphicon-plus"></i>
                </a>
              </span>
              <span class="clickable filter" data-toggle="tooltip" title="Toggle table filter" data-container="body">
                <i class="glyphicon glyphicon-filter"></i>
              </span>
            </div>
          </div>
          <div class="panel-body">
            <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter" />
          </div>
          <table class="table table-hover table-condensed" id="dev-table">
            <thead>
              <tr>
                <th>#</th>
                <th>Description</th>
                <th>Active</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @foreach ($records as $record)
                <tr>
                  <td>{{ HTML::linkRoute('brand.edit', $record->id, [$record->id]) }}</td>
                  <td> {{ $record->descr }} </td>
                  <td>{{ Form::checkbox('active', $record->active, $record->active, array('disabled' => 'disabled')) }}</td>



                  <td> @include ('partials.deletebutton', ['destroyRoute' => 'brand.destroy', 'destroyId' => $record->id]) </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
@stop