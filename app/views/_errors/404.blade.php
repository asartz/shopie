@extends('layouts.master')

@section('title')
<title>404 - Page not fount</title>
@stop

@section('content')

<!-- BEGIN CONTAINER -->

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="span5 number">
            404
         </div>
         <div class="span7 details">
            <h3>Sorry        <small>but we can't find what you are looking for!</small></h3>
            <p>
               Url /{{Request::path()}} was not found on this server</strong></a>.<br />
            </p>
         </div>
    </div>
</div>

<!-- END CONTAINER -->
@stop
