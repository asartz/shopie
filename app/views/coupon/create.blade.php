  @extends('layouts.master')

  @section('content')
    <div class="row">
      <div class="col-md-8 col-md-offset-2">

        {{ Form::open( ['route' => ['coupon.store'], 'class'=>'form-horizontal', 'role' => 'form'] ) }}
          <fieldset>

            <!-- Form Name -->
            <legend>Coupon :: New Record</legend>

            <div class="form-group">
              <label class="col-sm-2 control-label" for="textinput">Description</label>
              <div class="col-sm-10">
                {{ Form::text('descr', null, ['class' => 'form-control']) }}
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label" for="textinput">Valid Date From</label>
              <div class="col-sm-10">
                {{ Form::text('validdatefrom', null, ['class' => 'form-control', 'placeholder' => 'dd/mm/yyyy']) }}
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label" for="textinput">Valid Date To</label>
              <div class="col-sm-10">
                {{ Form::text('validdateto', null, ['class' => 'form-control', 'placeholder' => 'dd/mm/yyyy']) }}
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label" for="textinput">Discount Price</label>
              <div class="col-sm-10">
                {{ Form::text('discountprice', null, ['class' => 'form-control']) }}
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label" for="textinput">Active</label>
              <div class="col-sm-10">
                 {{ Form::checkbox('active', 1, true) }}
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
              <a href="{{URL::route('coupon.index')}}" class="btn btn-default">Back</a>
                <div class="pull-right">
                  <button type="reset" class="btn btn-default">Cancel</button>
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </div>
            </div>

          </fieldset>
          {{ Form::close() }}

      </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->
@stop