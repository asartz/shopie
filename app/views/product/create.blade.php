  @extends('layouts.master')

  @section('content')
    <div class="row">
      <div class="col-md-8 col-md-offset-2">

        {{ Form::open( ['route' => ['product.store'], 'class'=>'form-horizontal', 'role' => 'form'] ) }}
          <fieldset>

            <!-- Form Name -->
            <legend>Product :: New Record</legend>

            <div class="form-group">
              <label class="col-sm-2 control-label">Category</label>
              <div class="col-sm-4">
                {{ Form::select('category_id', $categories, null, ['class' => 'form-control', 'required', 'id' => 'category_id']) }}
              </div>

              <label class="col-sm-2 control-label" for="cameraman_id">Brands</label>
              <div class="col-sm-4">
                {{ Form::select('brand_id', $brands, null, ['class' => 'form-control', 'required', 'id' => 'brand_id']) }}
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label" for="textinput">Model Title</label>
              <div class="col-sm-4">
                {{ Form::text('modeltitle', null, ['class' => 'form-control']) }}
              </div>

              <label class="col-sm-2 control-label" for="textinput">Reference Id</label>
              <div class="col-sm-4">
                {{ Form::text('referenceid', null, ['class' => 'form-control']) }}
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label" for="textinput">Price</label>
              <div class="col-sm-4">
                {{ Form::text('price', null, ['class' => 'form-control']) }}
              </div>

              <label class="col-sm-2 control-label" for="textinput">Special Price</label>
              <div class="col-sm-4">
                {{ Form::text('specialprice', null, ['class' => 'form-control']) }}
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label" for="remarks">Remark</label>
              <div class="col-sm-10">
                {{ Form::textarea('remarks', null, array('class' => 'form-control',  'rows' => '4', 'maxlength' => '2000', 'id' => 'remarks')) }}
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label" for="textinput">Sort Order</label>
              <div class="col-sm-4">
                {{ Form::text('sortorder', null, ['class'=>'form-control', 'required' => 'true']) }}
              </div>

              <label class="col-sm-2 control-label" for="textinput">Active</label>
              <div class="col-sm-4">
                 {{ Form::checkbox('active', 1, true) }}
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
              <a href="{{URL::route('product.index')}}" class="btn btn-default">Back</a>
                <div class="pull-right">
                  <button type="reset" class="btn btn-default">Cancel</button>
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </div>
            </div>

          </fieldset>
          {{ Form::close() }}

      </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->
@stop