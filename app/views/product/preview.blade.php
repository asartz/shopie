@extends('layouts.master')

@section('title')
        <title>{{ $record->category->descr }} {{ $record->brand->descr }} {{$record->modeltitle}} - {{Config::get('shopie.CLIENT_FIRM')}}</title>
@endsection

@section('meta')
    <meta content="{{ $record->category->descr }},{{ $record->brand->descr }},{{$record->modeltitle}}" name="keywords" />
    <meta content="{{$record->remarks or $record->modeltitle}}" name="description" />
@endsection

@section('headscripts')

@stop

@section('content')
 <!-- Page Header -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Product Info
            <small> {{ $record->category->descr }} :: {{ $record->brand->descr }} </small>
        </h1>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="col-xs-12 col-sm-12 col-md-5">
            <div class="product col-md-12 service-image-left">
                <img id="item-display" class="img-responsive" src="http://placehold.it/300x200" alt=""></img>
            </div>
        </div>

        <div class="col-md-7">
            <div class="product-title">{{$record->modeltitle}}</div>
            <div class="product-title">Ref.:{{$record->referenceid}}</div>
            <div class="product-desc">{{$record->remarks}}</div>
            <div class="product-rating"><i class="fa fa-star gold"></i> <i class="fa fa-star gold"></i> <i class="fa fa-star gold"></i> <i class="fa fa-star gold"></i> <i class="fa fa-star-o"></i> </div>
            <hr>
            <div class="product-price">$
                @if (empty($record->specialprice))
                    <b>{{$record->price}}</b>
                @else
                    <strike>{{$record->price}}</strike> <b>{{$record->specialprice}}</b>
                @endif
            </div>
            <div class="product-stock">In Stock</div>
            <hr>
            <div class="btn-group cart">
                <a href="{{URL::route('cartadd', [$record->slug])}}" class="btn btn-success"> Add to cart</a>
            </div>
            {{-- TODO: Implement a wish list
            <div class="btn-group wishlist">
                <button type="button" class="btn btn-danger">
                    Add to wishlist
                </button>
            </div>
            --}}
        </div>
    </div>
</div>

<div class="row" style="padding-top: 40px;">
    <h4>Other items</h4>

    <div class='col-md-12'>
        <div class="row">
            @foreach ($otheritems as $otheritem)
                <div class="col-md-4">
                    <a href="{{URL::route('preview', [$otheritem->slug])}}"><img alt="" src="http://placehold.it/150x150">
                        <div class="info">
                            <div class="price col-md-12">
                                <h5>
                                    {{$otheritem->brand->descr}}</h5>
                                <h5>
                                    {{$otheritem->modeltitle}}</h5>
                                <h5 class="price-text-color">
                                    <b>€</b>
                                    @if (empty($otheritem->specialprice))
                                        <b>{{$otheritem->price}}</b>
                                    @else
                                        <strike>{{$otheritem->price}}</strike> <b>{{$otheritem->specialprice}}</b>
                                    @endif</h5>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>

@stop