@extends('layouts.master')

@section('title')
    @if (!Input::has('category_id') and !Input::has('subcategory_id'))
        {{-- do nothing --}}
        @parent
    @elseif (Input::has('category_id') and !Input::has('subcategory_id'))
        <title>{{ $categories[0]->descr }} - Buy watches online - {{Config::get('shopie.CLIENT_FIRM')}}</title>
    @elseif (Input::has('category_id') and Input::has('subcategory_id'))
        <title>{{ $categories[0]->descr }} :: {{ $subcategory->descr }} - Buy watches online - {{Config::get('shopie.CLIENT_FIRM')}}</title>
    @endif
@endsection

@section('meta')
    @if (!Input::has('category_id') and !Input::has('subcategory_id'))
        {{-- do nothing --}}
        @parent
    @elseif (Input::has('category_id') and !Input::has('subcategory_id'))
        <meta content="{{ $categories[0]->descr }}, videos, επικαιρότητα, ειδήσεις" name="keywords" />
        <meta content="Videos {{ $categories[0]->descr }}" name="description" />
    @elseif (Input::has('category_id') and Input::has('subcategory_id'))
        <meta content="{{ $categories[0]->descr }}, {{ $subcategory->descr }}, videos, επικαιρότητα, ειδήσεις" name="keywords" />
        <meta content="Videos {{ $categories[0]->descr }} {{ $subcategory->descr }}" name="description" />
    @endif
@endsection

@section('content')
 <!-- Page Header -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><a href="{{ URL::to('/') }}">Products List</a>
            <small>@if (!Input::has('category_id') and !Input::has('brand_id')) All @elseif (Input::has('category_id') and !Input::has('brand_id')) {{ $categories[0]->descr }} @elseif (!Input::has('category_id') and Input::has('brand_id')) {{ $brands[0]->descr }} @elseif (Input::has('category_id') and Input::has('brand_id')) {{ $categories[0]->descr }} :: {{ $brands[0]->descr }} @endif</small>
        </h1>
    </div>
</div>
<!-- /.row -->
<div class="row">
	<div class="col-xs-12 col-sm-3 col-md-3">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">BROWSE</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <label class="control-label">Categories</label>
                            <ul class="list-unstyled">
                              <li>
                                <a href="{{ URL::route('index') }}">All</a>
                              </li>
                            @foreach ($categories as $category)
                              <li>
                                <a href="{{URL::route('index')}}?category_id={{$category->id}}">{{ $category->descr }}</a>
                              </li>
                            @endforeach
                        </ul>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <label class="control-label">Brands</label>
                            <ul class="list-unstyled">
                              <li>
                                <a href="{{ URL::route('index') }}">All</a>
                              </li>
                            @foreach ($brands as $brand)
                              <li>
                                <a href="{{URL::route('index')}}?brand_id={{$brand->id}}">{{ $brand->descr }}</a>
                              </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            </div>
	   </div>

	<div class="col-xs-12 col-sm-9 col-md-9">
        <!-- Products Row -->

            @foreach ($records as $key => $record)
                @if ($key % $itemsperrow === 0) <div class="row margin-bottom-5"> @endif
                    <div class="col-md-4">
                        <div class="col-item">
                            <div class="photo">
                                <a href="{{URL::route('preview', [$record->slug])}}"><img class="img-responsive" src="http://placehold.it/300x200"></a>
                            </div>
                            <div class="info">
                                <div class="row">
                                    <div class="price col-md-12">
                                        <h5>
                                            {{$record->brand->descr}}</h5>
                                        <h5>
                                            {{$record->modeltitle}}</h5>
                                        <h5 class="price-text-color">
                                            <b>€</b>
                                            @if (empty($record->specialprice))
                                                <b>{{$record->price}}</b>
                                            @else
                                                <strike>{{$record->price}}</strike> <b>{{$record->specialprice}}</b>
                                            @endif</h5>
                                    </div>
                                </div>
                                <div class="separator clear-left">
                                    <p class="btn-add">
                                        <i class="fa fa-shopping-cart"></i><a href="{{URL::route('cartadd', [$record->slug])}}" class="add"> Add to cart</a></p>
                                    <p class="btn-details">
                                        <i class="fa fa-list"></i><a href="{{URL::route('preview', [$record->slug])}}" class="preview"> More details</a></p>
                                </div>
                                <div class="clearfix">
                                </div>
                            </div>
                        </div>
                    </div>
                @if ($key === ($itemsperrow - 1) or $key % $itemsperrow === ($itemsperrow - 1) or $key === count($records) - 1) </div> @endif
            @endforeach

        <!-- /.row -->
        @if (count($records) > 0)
            <hr>
            <!-- Pagination -->
            <div class="row text-center">
                <div class="col-lg-12">
                    {{ $records->appends(['category_id' => Input::get('category_id'), 'brand_id' => Input::get('brand_id')])->links() }}
                </div>
            </div>
            <!-- /.row -->
        @endif
    </div>
</div>
@stop