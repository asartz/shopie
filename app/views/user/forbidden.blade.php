@extends('layouts.master')

@section('content')
<div class="alert alert-danger">
  <p><strong>You do not have access to this area.</strong></p>
</div>
@stop