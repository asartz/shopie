@extends('layouts.master')

@section('title')
        <title>Profile</title>
@endsection

@section('meta')
    <meta content="profile" name="keywords" />
    <meta content="Profile" name="description" />
@endsection

<style>
   .form-control { margin-bottom: 10px; }
</style>

@section('content')
 <!-- Page Header -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Profile
        </h1>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-6">
          <legend><i class="fa fa-user"></i> Acount Info</legend>
            {{ Form::open( array('route' => array('postprofile'), 'role' => 'form') ) }}
              <div class="form-group">
                <div class="col-sm-12">
                  {{ Form::text('fullname', Auth::user()->username, ['class' => 'form-control', 'placeholder' => 'Full name', 'disabled' => 'true']) }}
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-12">
                  {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) }}
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-12">
                  {{ Form::password('retypepassword', ['class' => 'form-control', 'placeholder' => 'Retype Password']) }}
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-12">
                  {{ Form::email('email', Auth::user()->email, ['class' => 'form-control', 'placeholder' => 'Email']) }}
                </div>
              </div>
        </div>

        <div class="col-md-6">
        <legend><i class="fa fa-envelope"></i> Shipping Address</legend>
            <div class="form-group">
              <div class="col-sm-12">
                {{ Form::text('fullname',(!empty($shippingaddress)) ? $shippingaddress->fullname : null, ['class' => 'form-control', 'placeholder' => 'Full name']) }}
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-12">
                {{ Form::text('streetaddress',(!empty($shippingaddress)) ? $shippingaddress->streetaddress : null, ['class' => 'form-control', 'placeholder' => 'Street Address']) }}
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-12">
                {{ Form::text('streetno',(!empty($shippingaddress)) ? $shippingaddress->streetno : null, ['class' => 'form-control', 'placeholder' => 'Street No']) }}
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-12">
                {{ Form::text('region',(!empty($shippingaddress)) ? $shippingaddress->region : null, ['class' => 'form-control', 'placeholder' => 'Region']) }}
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-12">
                {{ Form::text('city',(!empty($shippingaddress)) ? $shippingaddress->city : null, ['class' => 'form-control', 'placeholder' => 'City']) }}
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-12">
                {{ Form::text('zipcode',(!empty($shippingaddress)) ? $shippingaddress->zipcode : null, ['class' => 'form-control', 'placeholder' => 'Zipcode']) }}
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-12">
                {{ Form::text('phone',(!empty($shippingaddress)) ? $shippingaddress->phone : null, ['class' => 'form-control', 'placeholder' => 'Phone']) }}
              </div>
            </div>

            <br />
            <legend><i class="fa fa-envelope"></i> Billing Address</legend>

            <div class="form-group">
              <div class="col-sm-12">
                {{ Form::text('billfullname',(!empty($billingaddress)) ? $billingaddress->fullname : null, ['class' => 'form-control', 'placeholder' => 'Full name']) }}
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-12">
                {{ Form::text('billstreetaddress',(!empty($billingaddress)) ? $billingaddress->streetaddress : null, ['class' => 'form-control', 'placeholder' => 'Street Address']) }}
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-12">
                {{ Form::text('billstreetno',(!empty($billingaddress)) ? $billingaddress->streetno : null, ['class' => 'form-control', 'placeholder' => 'Street No']) }}
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-12">
                {{ Form::text('billregion',(!empty($billingaddress)) ? $billingaddress->region : null, ['class' => 'form-control', 'placeholder' => 'Region']) }}
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-12">
                {{ Form::text('billcity',(!empty($billingaddress)) ? $billingaddress->city : null, ['class' => 'form-control', 'placeholder' => 'City']) }}
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-12">
                {{ Form::text('billzipcode',(!empty($billingaddress)) ? $billingaddress->zipcode : null, ['class' => 'form-control', 'placeholder' => 'Zipcode']) }}
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-12">
                {{ Form::text('billphone',(!empty($billingaddress)) ? $billingaddress->phone : null, ['class' => 'form-control', 'placeholder' => 'Phone']) }}
              </div>
            </div>

            <br />
            <br />

          </div>
        {{ Form::submit('Update Profile', ['class' => 'btn btn-md btn-success btn-block']) }}
      {{ Form::close() }}
    </div>
</div>

@stop