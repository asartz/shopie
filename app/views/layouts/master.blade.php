<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="author" content="{{Config::get('shopie.CLIENT_FIRM')}}" />
    <link rel="icon" href="../../favicon.ico" />

    @section('title')
     <title>Your little shop on the web - {{Config::get('shopie.CLIENT_FIRM')}}</title>
    @show

    @section('meta')
      <meta content="buy, watch" name="keywords" />
      <meta content="Buy watches online. Best prices. High quality." name="description" />
    @show

    {{ HTML::style('assets/bower_vendor/bootstrap/dist/css/bootstrap.min.css') }}

    <!-- Bootstrap core CSS -->
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">-->
    <!-- Optional theme -->
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">-->

    <!-- Font awesome -->
    {{ HTML::style('assets/bower_vendor/font-awesome/css/font-awesome.min.css') }}

    @yield('styles')

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom CSS -->
    {{ HTML::style('assets/custom/css/master.css') }}

    <!-- Head positioned scripts -->
    @yield('headscripts')
  </head>

  <body style="min-height: 768px; padding-top: 70px;">

    @section('navbar')
        @include('partials.navbar')
    @show

    <div class="container">
        @include('partials.errors')
        @yield('content')
        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p style="text-align:center;">Powered by {{Config::get('shopie.CLIENT_FIRM')}}&copy;<br>
                    Built with <i class="fa fa-heart"></i> by @asartz</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>
    </div> <!-- /container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    {{ HTML::script('assets/custom/js/ie10-viewport-bug-workaround.js') }}

    @yield('scripts')

  </body>
</html>
