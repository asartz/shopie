@extends('layouts.master')

@section('styles')
  {{ HTML::style('assets/custom/css/table.css') }}
@stop

@section('scripts')
  {{ HTML::script('assets/custom/js/table.js') }}
@stop

@section('content')
<div class="row">
      <div class="col-xs-12 col-sm-3 col-md-3">
              {{ Form::open( ['route' => ['loginparam.index'], 'method' => 'GET', 'files' => false, 'class' => 'form-horizontal', 'role' => 'form'] ) }}
              <div class="panel panel-primary">
                  <div class="panel-heading">
                      <h3 class="panel-title">Search Criteria</h3>
                  </div>
                  <div class="panel-body">
                      <div class="row">
                          <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <label class="control-label">Date from</label>
                            {{ Form::text('dtin', Input::get('dtin', null), ['class' => 'form-control', 'placeholder' => 'DD/MM/YYYY']) }}
                        </div>
                        <div class="form-group">
                            <label class="control-label">Date to</label>
                            {{ Form::text('dtto', Input::get('dtto', null), ['class' => 'form-control', 'placeholder' => 'DD/MM/YYYY']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::select('user_id', $users, Input::get('user_id', null), ['id' => 'user_id', 'class' => 'form-control']) }}
                        </div>
                          </div>
                      </div>
                  </div>
                  <div class="panel-footer">
                      <div class="row">
                          <div class="col-xs-12 col-sm-12 col-md-12">
                              <button type="submit" class="btn btn-labeled btn-success btn-block">
                                  <span class="btn-label"><i class="glyphicon glyphicon-ok"></i></span> Search</button>
                          </div>
                      </div>
                  </div>
              </div>
              {{ Form::close() }}
      </div>

      <div class="col-xs-12 col-sm-9 col-md-9">
        <div class="panel panel-primary panel-table">
          <div class="panel-heading">
            <h3 class="panel-title">Logins</h3>
            <div class="pull-right">
              <span class="clickable filter" data-toggle="tooltip" title="Toggle table filter" data-container="body">
                <i class="glyphicon glyphicon-filter"></i>
              </span>
            </div>
          </div>
          <div class="panel-body">
            <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter" />
          </div>
          <table class="table table-hover table-condensed" id="dev-table">
            <thead>
              <tr>
                <th>User</th>
                <th>Login datetime</th>
                <th>Logout datetime</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @foreach ($records as $record)
                <tr>
                  <td>{{ $record->user->username . ' (' . $record->user->lastname . ' ' . $record->user->firstname . ')' }}</td>
                  <td>{{ $record->created_at }}</td>
                  <td>{{ $record->logout_at }}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      {{ $records->links() }}
      </div>
</div>
@stop

