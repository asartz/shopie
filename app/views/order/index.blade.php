@extends('layouts.master')

@section('title')
        <title>My Orders - {{Config::get('shopie.CLIENT_FIRM')}}</title>
@endsection

@section('meta')
    <meta content="my orders" name="keywords" />
    <meta content="my orders" name="description" />
@endsection

@section('content')
 <!-- Page Header -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">My Orders
        </h1>
    </div>
</div>
<div class="row">
    <div class="container">
        <div class="row col-md-12 custyle">
            @foreach ($records as $record)
                <table class="table table-striped custab">
                    <thead>
                        <tr>
                            <th>Order Id - Customer name</th>
                            <th>Date</th>
                            <th>Discount</th>
                            <th>Cost</th>
                        </tr>
                    </thead>
                        <tr>
                            <td>{{$record->id}} - {{$record->user->fullname}}</td>
                            <td>{{$record->datecreated}}</td>
                            <td>{{$record->discountvalue ?: 0}}</td>
                            <td>{{$record->totalcost ?: 0}}</td>
                        </tr>
                        <tr>
                            <td><strong>Product</strong></td>
                            <td><strong>Quantity</strong></td>
                            <td><strong>Price</strong></td>
                            <td><strong>Cost</strong></td>
                        </tr>
                        @foreach ($record->orderdetails as $orderdetail)
                            <tr>
                                <td>{{$orderdetail->product->brand->descr}} {{$orderdetail->product->modeltitle}}</td>
                                <td>{{$orderdetail->quantity}}</td>
                                <td>{{$orderdetail->price ?: 0}}</td>
                                <td>{{$orderdetail->rowcost ?: 0}}</td>
                            </tr>
                        @endforeach
                </table>
            @endforeach
        </div>
    </div>
</div>

@stop