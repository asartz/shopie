<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingaddressesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('billingaddresses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('fullname', 255);
			$table->string('streetaddress', 255);
			$table->string('streetno', 30);
			$table->string('region', 255);
			$table->string('city', 255);
			$table->string('zipcode', 30);
			$table->string('phone', 30);
			$table->timestamps();

			// create foreign keys
			$table->foreign('user_id')->references('id')->on('users')->onDelete('restrict')->onUpdate('restrict');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('billingaddresses', function(Blueprint $table)
		{
			$table->drop();
		});
	}

}
