<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderdetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orderdetails', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('order_id');
			$table->integer('product_id');
			$table->integer('quantity');
			$table->decimal('price', 19, 2);
			$table->decimal('rowcost', 19, 2);
			$table->timestamps();

			// create foreign keys
			$table->foreign('order_id')->references('id')->on('orders')->onDelete('restrict')->onUpdate('restrict');
			$table->foreign('product_id')->references('id')->on('products')->onDelete('restrict')->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('orderdetails', function(Blueprint $table)
		{
			$table->drop();
		});
	}

}
