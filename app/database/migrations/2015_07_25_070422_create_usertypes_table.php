<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsertypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usertypes', function(Blueprint $table)
		{
			$table->integer('id');
			$table->string('identifier', 255);
			$table->string('descr', 255);
			$table->integer('sortorder');
			$table->text('remarks')->nullable();
			$table->timestamps();

			//create primary key
			$table->primary('id');

			// create indexes
			$table->unique('descr');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('usertypes');
	}

}
