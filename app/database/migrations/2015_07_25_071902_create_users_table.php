<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('username', 30);
			$table->string('password', 150);
			$table->string('email', 100);
			$table->string('fullname', 150);
			$table->string('remarks', 500)->nullable();
			$table->integer('usertype_id');
			$table->boolean('active');

			$table->rememberToken();
			$table->timestamps();

			// create indexes
			$table->unique('username');
			$table->unique('email');

			// create foreign keys
			$table->foreign('usertype_id')->references('id')->on('usertypes')->onDelete('restrict')->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}

}
