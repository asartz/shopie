<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('coupon_id')->nullable();
			$table->decimal('cartvalue', 19, 2);
			$table->decimal('discountvalue', 19, 2)->nullable();
			$table->decimal('totalcost', 19, 2);
			$table->timestamp('datecreated');
			$table->timestamps();

			// create foreign keys
			$table->foreign('user_id')->references('id')->on('users')->onDelete('restrict')->onUpdate('restrict');
			$table->foreign('coupon_id')->references('id')->on('coupons')->onDelete('restrict')->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('orders', function(Blueprint $table)
		{
			$table->drop();
		});
	}

}
