<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('category_id');
			$table->integer('brand_id');
			$table->string('modeltitle', 255);
			$table->string('referenceid', 255);
			$table->decimal('price', 19, 2);
			$table->decimal('specialprice', 19, 2)->nullable();
			$table->text('remarks')->nullable();
			$table->boolean('active');
			$table->integer('sortorder');
			$table->timestamps();

			// create indexes
			$table->index('category_id');
			$table->index('brand_id');

			// create foreign keys
			$table->foreign('category_id')->references('id')->on('categories')->onDelete('restrict')->onUpdate('restrict');
			$table->foreign('brand_id')->references('id')->on('brands')->onDelete('restrict')->onUpdate('restrict');
		});

		// add special column to hold full text search data
		DB::statement
		(
			"ALTER TABLE IF EXISTS products ADD COLUMN fts tsvector;"
		);

		// create index for the special column of full text search data
		DB::statement
		(
			"CREATE INDEX products_fts_index on products USING GIN (fts);"
		);

		// fts triger
		DB::statement
		(
			"CREATE FUNCTION products_fts_tr() RETURNS trigger AS
            \$body\$
            BEGIN
            new.fts = to_tsvector('pg_catalog.simple', coalesce(new.modeltitle, ''));
            RETURN NEW;
            END;
            \$body\$
            LANGUAGE 'plpgsql'
            VOLATILE
            CALLED ON NULL INPUT
            SECURITY INVOKER;"
		);

		DB::statement
		(
			"CREATE TRIGGER products_fts_tr
            BEFORE INSERT OR UPDATE
            ON public.products FOR EACH ROW
            EXECUTE PROCEDURE products_fts_tr();"
		);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// fts triger
		DB::statement("DROP TRIGGER IF EXISTS products_fts_tr on products;");
		DB::statement("DROP FUNCTION IF EXISTS products_fts_tr();");

		Schema::table('products', function(Blueprint $table)
		{
			$table->drop();
		});
	}

}
