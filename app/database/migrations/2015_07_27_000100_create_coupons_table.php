<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('coupons', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('descr', 255);
			$table->timestamp('validdatefrom');
			$table->timestamp('validdateto');
			$table->decimal('discountprice', 19, 2);
			$table->boolean('active');
			$table->timestamps();

			// create indexes
			$table->unique('descr');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('coupons', function(Blueprint $table)
		{
			$table->drop();
		});
	}

}
