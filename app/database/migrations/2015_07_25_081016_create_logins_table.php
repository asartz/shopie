<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoginsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('logins', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('sessionid', 255);
			$table->string('created_ip', 50);
			$table->string('updated_ip', 50)->nullable();
			$table->timestamp('logout_at')->nullable();
			$table->timestamps();

			$table->index('user_id');
			$table->index('sessionid');
			$table->index('created_at');

			// create foreign keys
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('logins');
	}

}
