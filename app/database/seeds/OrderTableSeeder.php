<?php

use Illuminate\Database\Seeder;

class OrderTableSeeder extends Seeder {

    public function run()
    {
        $data = array(
            array('user_id' => '3', 'coupon_id' => '1', 'cartvalue' => '2650', 'discountvalue' => '10', 'totalcost' => '2550', 'datecreated' => '24/7/2015 10:52:10', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
            array('user_id' => '3', 'coupon_id' => null, 'cartvalue' => '130', 'discountvalue' => null, 'totalcost' => '130', 'datecreated' => '25/7/2015 10:52:10', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
            array('user_id' => '3', 'coupon_id' => null, 'cartvalue' => '2227', 'discountvalue' => null, 'totalcost' => '2227', 'datecreated' => '26/7/2015 10:52:10', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
        );

        DB::table('orders')->delete();

        DB::table('orders')->insert($data);
    }

}