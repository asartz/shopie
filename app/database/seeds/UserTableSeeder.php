<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder {

	public function run()
	{
		$data = array(
			array('username' => 'admin', 'password' => Hash::make('secret'), 'email' => 'asartz@versus-software.gr', 'fullname' => 'Sartzetakis Antonios', 'remarks' => 'Master Admin Account', 'usertype_id' => 10, 'active' => '1', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
			array('username' => 'power', 'password' => Hash::make('secret'), 'email' => 'gsartz@versus-software.gr', 'fullname' => 'Sartzetakis George', 'remarks' => 'Power User Account', 'usertype_id' => 20, 'active' => '1', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
			array('username' => 'customer', 'password' => Hash::make('secret'), 'email' => 'info@versus-software.gr', 'fullname' => 'Nick Rhodes', 'remarks' => 'Simple User Account', 'usertype_id' => 30, 'active' => '1', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
		);

		DB::table('users')->delete();

		DB::table('users')->insert($data);
	}

}