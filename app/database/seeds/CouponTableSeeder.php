<?php

use Illuminate\Database\Seeder;

class CouponTableSeeder extends Seeder {

    public function run()
    {
        $data = array(
            array('descr' => '1111', 'validdatefrom' => '26/7/2015', 'validdateto' => '31/12/2015', 'discountprice' => '10', 'active' => '1', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
            array('descr' => '9999', 'validdatefrom' => '26/7/2015', 'validdateto' => '31/12/2015', 'discountprice' => '20', 'active' => '1', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
        );

        DB::table('coupons')->delete();

        DB::table('coupons')->insert($data);
    }

}