<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder {

    public function run()
    {
        $data = array(
            array('descr' => 'MEN WATCHES', 'active' => '1', 'sortorder' => '1', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'slug' => 'men-watches'),
            array('descr' => 'WOMEN WATCHES', 'active' => '1', 'sortorder' => '2', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'slug' => 'women-watches'),
            array('descr' => 'KIDS WATCHES', 'active' => '1', 'sortorder' => '1', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'slug' => 'kids-watches'),
        );

        DB::table('categories')->delete();

        DB::table('categories')->insert($data);
    }

}