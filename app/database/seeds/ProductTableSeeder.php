<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder {

	public function run()
	{
		$data = array(
			array('category_id' => '1', 'brand_id' => '1', 'modeltitle' => 'Sheen', 'referenceid' => 'SHE-3803D-7AUER', 'price' => '350', 'specialprice' => null, 'remarks' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc auctor ante ac diam vestibulum, a auctor orci posuere. Phasellus et maximus lectus, sit amet pulvinar ex.', 'active' => '1', 'sortorder' => '1', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'slug' => 'men-watches-casio-sheen-1'),
			array('category_id' => '1', 'brand_id' => '2', 'modeltitle' => 'Promaster Land Radio Sky', 'referenceid' => 'BY0011-50E', 'price' => '650', 'specialprice' => null, 'remarks' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc auctor ante ac diam vestibulum, a auctor orci posuere. Phasellus et maximus lectus, sit amet pulvinar ex.', 'active' => '1', 'sortorder' => '1', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'slug' => 'men-watches-citizen-promaster-land-radio-sky-2'),
			array('category_id' => '2', 'brand_id' => '3', 'modeltitle' => 'Ladies Cupcake', 'referenceid' => 'W70018L2', 'price' => '65', 'specialprice' => null, 'remarks' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc auctor ante ac diam vestibulum, a auctor orci posuere. Phasellus et maximus lectus, sit amet pulvinar ex.', 'active' => '1', 'sortorder' => '1', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'slug' => 'women-watches-guess-ladies-cupcake-3'),
			array('category_id' => '1', 'brand_id' => '4', 'modeltitle' => 'Gold Series', 'referenceid' => 'KG003', 'price' => '180', 'specialprice' => null, 'remarks' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc auctor ante ac diam vestibulum, a auctor orci posuere. Phasellus et maximus lectus, sit amet pulvinar ex.', 'active' => '1', 'sortorder' => '1', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'slug' => 'men-watches-kyboe-gold-series-4'),
			array('category_id' => '1', 'brand_id' => '5', 'modeltitle' => 'Chronograph Perpetual Alarm', 'referenceid' => 'SPC129P1', 'price' => '229', 'specialprice' => null, 'remarks' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc auctor ante ac diam vestibulum, a auctor orci posuere. Phasellus et maximus lectus, sit amet pulvinar ex.', 'active' => '1', 'sortorder' => '1', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'slug' => 'men-watches-seiko-chronograph-perpetual-alarm-5'),
			array('category_id' => '1', 'brand_id' => '6', 'modeltitle' => 'PR 100 Gent Chronograph', 'referenceid' => 'T101.417.22.031.00', 'price' => '475', 'specialprice' => '295', 'remarks' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc auctor ante ac diam vestibulum, a auctor orci posuere. Phasellus et maximus lectus, sit amet pulvinar ex.', 'active' => '1', 'sortorder' => '1', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'slug' => 'men-watches-tissot-pr-100-gent-chronograph-6'),
			array('category_id' => '3', 'brand_id' => '5', 'modeltitle' => 'CH 430 Baby Chronograph', 'referenceid' => 'T101.417.22.765.854', 'price' => '100', 'specialprice' => '95', 'remarks' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc auctor ante ac diam vestibulum, a auctor orci posuere. Phasellus et maximus lectus, sit amet pulvinar ex.', 'active' => '1', 'sortorder' => '1', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'slug' => 'kids-watches-seiko-ch-430-baby-chronograph-7'),
		);

		DB::table('products')->delete();

		DB::table('products')->insert($data);
	}

}