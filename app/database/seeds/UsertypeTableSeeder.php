<?php

use Illuminate\Database\Seeder;

class UserTypeTableSeeder extends Seeder {

	public function run()
	{
		$data = array(
			array('id' => 10, 'identifier' => 'USER_TYPE_ADMIN', 'descr' => 'ADMIN', 'sortorder' => '1', 'remarks' => 'Administrator', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
			array('id' => 20, 'identifier' => 'USER_TYPE_POWER_USER', 'descr' => 'POWER USER', 'sortorder' => '2', 'remarks' => 'Power User', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
			array('id' => 30, 'identifier' => 'USER_TYPE_CUSTOMER', 'descr' => 'CUSTOMER', 'sortorder' => '3', 'remarks' => 'Customer', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
		);

		DB::table('usertypes')->delete();

		DB::table('usertypes')->insert($data);
	}

}