<?php

use Illuminate\Database\Seeder;

class BrandTableSeeder extends Seeder {

    public function run()
    {
        $data = array(
            array('descr' => 'CASIO', 'active' => '1', 'sortorder' => '1', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'slug' => 'casio'),
            array('descr' => 'CITIZEN', 'active' => '1', 'sortorder' => '2', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'slug' => 'citizen'),
            array('descr' => 'GUESS', 'active' => '1', 'sortorder' => '3', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'slug' => 'guess'),
            array('descr' => 'KYBOE', 'active' => '1', 'sortorder' => '4', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'slug' => 'kyboe'),
            array('descr' => 'SEIKO', 'active' => '1', 'sortorder' => '5', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'slug' => 'seiko'),
            array('descr' => 'TISSOT', 'active' => '1', 'sortorder' => '6', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'slug' => 'tissot'),
        );

        DB::table('brands')->delete();

        DB::table('brands')->insert($data);
    }

}