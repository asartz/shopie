<?php

use Illuminate\Database\Seeder;

class OrderdetailTableSeeder extends Seeder {

    public function run()
    {
        $data = array(
            array('order_id' => '1', 'product_id' => '1', 'quantity' => '2', 'price' => '350', 'rowcost' => '700', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
            array('order_id' => '1', 'product_id' => '2', 'quantity' => '3', 'price' => '650', 'rowcost' => '1950', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),

            array('order_id' => '2', 'product_id' => '3', 'quantity' => '2', 'price' => '65', 'rowcost' => '130', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),

            array('order_id' => '3', 'product_id' => '4', 'quantity' => '2', 'price' => '180', 'rowcost' => '360', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
            array('order_id' => '3', 'product_id' => '5', 'quantity' => '3', 'price' => '229', 'rowcost' => '687', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
            array('order_id' => '3', 'product_id' => '6', 'quantity' => '4', 'price' => '295', 'rowcost' => '1180', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),

        );

        DB::table('orderdetails')->delete();

        DB::table('orderdetails')->insert($data);
    }

}